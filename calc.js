var Fleet  = Ember.Object.extend({
    	ships : '0',
		tech : '1',
		postBattleShips : 0,
    onShipsUpdate : function(){
    	App.updateUI(App.calc());
    }.observes('ships')
});

window.App = Ember.Application.create({
	attacker : Fleet.create({
		ships : '1'
	}),
	defender : Fleet.create({
		ships : '0'
	}),
	calc : function(){
         
		var 
			aShips = parseInt(this.attacker.ships),
			dShips = parseInt(this.defender.ships),
			aStr = parseInt(this.attacker.tech) + 1,
			dStr = parseInt(this.defender.tech) + 2;
			while( aShips > 0 && dShips > 0 ){
				aShips -= dStr;
				if(aShips > 0){
					dShips -= aStr;
				}
			}
            this.attacker.set('postBattleShips', aShips);
            this.attacker.set('postBattleShips', dShips);

			if( this.attacker.ships == 0 ){
				return "Attacker has no ships, thus no battle.";
			}else if( aShips > 0 && this.attacker.ships > 0 ){
				return "Attacker wins with "+ (aShips >= 0 ? aShips : 0)+" ships remaining.";
			}else{
				return "Defender wins with " + (dShips >= 0 ? dShips : 0) + " ships remaining.";
			}
	},
    updateUI : function(res){
        var el = document.getElementById('outcome');
    	el.innerHTML = res;
	}, 
});

$(document).ready(function(){
    var attackerShipsView = Ember.TextField.create({
        valueBinding : "App.attacker.ships"
    });
    attackerShipsView.appendTo($('#t-aships'));
});
